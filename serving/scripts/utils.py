def convert_nanoseconds_to_seconds(ns):
    return ns/(1**(-9))

def bytes_to_gb(bytes):
    return bytes/(1024 ** 3)