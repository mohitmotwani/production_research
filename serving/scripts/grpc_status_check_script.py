from tensorflow_serving.apis import get_model_metadata_pb2
from tensorflow_serving.apis import prediction_service_pb2_grpc
from tensorflow_serving.apis import predict_pb2
import grpc


host = "172.31.3.223"
port = "8502"
MAX_GRPC_MESSAGE_LENGTH = 512 * 1920 * 1920
options = [
            ('grpc.max_send_message_length', MAX_GRPC_MESSAGE_LENGTH),
            ('grpc.max_receive_message_length', MAX_GRPC_MESSAGE_LENGTH)
]
channel = grpc.insecure_channel(f"{host}:{port}", options=options)

request = get_model_metadata_pb2.GetModelMetadataRequest()
request.model_spec.name = "bg_model"
request.model_spec.signature_name = "serving_default"
request.metadata_field.append("signature_def")
stub = prediction_service_pb2_grpc.PredictionServiceStub(channel)
_ = stub.GetModelMetadata(request)

import serving.scripts.health_check_pb2 as health_check_pb2
import serving.scripts.health_check_pb2_grpc as health_check_pb2_grpc

health_check_stub = health_check_pb2_grpc.HealthStub(channel)
health_check_request = health_check_pb2.HealthCheckRequest()
health_check_stub.Check(health_check_request)
