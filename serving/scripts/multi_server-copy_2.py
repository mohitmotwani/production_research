import os
import threading
import logging
import json
import requests
import cv2
import numpy as np

headers = {"content-type": "application/json"}

import serving.scripts.bg_utils as bg_utils
import serving.scripts.umbilicus_utils as umb_utils
import serving.scripts.body_parts_utils as bp_utils

os.environ["TF_CPP_MIN_LOG_LEVEL"] = "2"
os.environ['TF_FORCE_GPU_ALLOW_GROWTH'] = 'true'

session = requests.Session()

with open("serving/configs/input/multi_serving_config.json", 'r') as f:
    config = json.load(f)
port = config['port']
body_parts_config = config["body_parts_config"]

logging_format = "[%(asctime)s] %(levelname)s - %(threadName)s %(message)s"
logging.basicConfig(filename=f'serving/temp/logs/multi_video_serving_log_{port}.log', level=logging.DEBUG, format=logging_format, datefmt="%d-%m-%Y %H:%M:%S")

def make_predictions(data, model_name, host="1", port=8503):
    host_address = config[f'host_address_{host}']
    json_response = session.post(
                f'http://{host_address}:{port}/v1/models/{model_name}:predict', data=data, headers=headers)
    _ = json.loads(json_response.text)['predictions']
    logging.info(f"Done Umb Prediction {host}")


def bg_pipeline(img, alpha=0.7, maintain_resolution=False):
    _, image, _, _, _ = bg_utils.prepare_inputs(img, maintain_resolution=False)
    data = json.dumps({"signature_name": "serving_default",
                      "instances": image[None, ...].numpy().tolist()})
    thread_1 = threading.Thread(target=make_predictions, args=(data, "bg_model", "1"))
    logging.debug("Starting first bg thread")
    thread_1.start()
    # thread_2 = threading.Thread(target=make_predictions, args=(data, "bg_model", "2"))
    # logging.debug("Starting second bg thread")
    # thread_2.start()

    
    logging.debug("Joining first child thread")
    thread_1.join()
    # logging.debug("Joining second child thread")
    # thread_2.join()

    logging.debug("Done with bg pipeline")


def detect_umb(img, top_n=10):
    input_tensor = umb_utils.preprocess_img(img)

    data = json.dumps({"signature_name": "serving_default",
                "instances": input_tensor.numpy().tolist()})
    
    thread_1 = threading.Thread(target=make_predictions, args=(data, "umbilicus" ,"1"))
    logging.debug("Starting first umb thread")
    thread_1.start()
    # thread_2 = threading.Thread(target=make_predictions, args=(data, "umbilicus", "2"))
    # logging.debug("Starting second umb thread")
    # thread_2.start()

    
    logging.debug("Joining first umb thread")
    thread_1.join()
    # logging.debug("Joining second umb thread")
    # thread_2.join()

    logging.debug("Done with umb pipeline")

def body_parts_pipeline(cvMat):
    params = body_parts_config['params']
    model_params = body_parts_config['model_params']
    # model = dnn_model['model']

    oriImg = cvMat.copy()
    flipImg = cv2.flip(oriImg, 1)
    oriImg = (oriImg / 256.0) - 0.5
    flipImg = (flipImg / 256.0) - 0.5
    multiplier = [x for x in params['scale_search']]

    segmap_scale = [None] * 8

    segmap_scale[3] = np.zeros(oriImg.shape[0] * oriImg.shape[1] * params["seg_num"]
                                ).reshape(oriImg.shape[0], oriImg.shape[1], params["seg_num"])

    segmap_scale[7] = np.zeros(oriImg.shape[0] * oriImg.shape[1] * params["seg_num"]
                                ).reshape(oriImg.shape[0], oriImg.shape[1], params["seg_num"])

    # tb._SYMBOLIC_SCOPE.value = True
    # bl._DISABLE_TRACKING.value = True

    threads = []
    for m in range(len(multiplier)):
        scale = multiplier[m]
        imageToTest = cv2.resize(
            oriImg, (0, 0), fx=scale, fy=scale, interpolation=cv2.INTER_CUBIC)

        pad = [0,
                0,
                (imageToTest.shape[0] - model_params['stride']
                ) % model_params['stride'],
                (imageToTest.shape[1] - model_params['stride']
                ) % model_params['stride']
                ]

        imageToTest_padded = np.pad(imageToTest, ((0, pad[2]), (0, pad[3]), (
            0, 0)), mode='constant', constant_values=((0, 0), (0, 0), (0, 0)))

        input_img = imageToTest[np.newaxis, ...]

        # TODO: tf serving
        data = json.dumps({"signature_name": "serving_default",
                    "instances": input_img.tolist()})
        # output_blobs = model.predict(input_img)
        logging.debug("Creating 2 body parts threads - first")
        thread_1 = threading.Thread(target=make_predictions, args=(data, "body_parts", "1"))
        thread_1.start()
        # thread_2 = threading.Thread(target=make_predictions, args=(data, "body_parts", "2"))
        # thread_2.start()
        threads.append(thread_1)

    logging.debug("Joining first set of body parts threads")
    for th in threads:
        th.join()


    # flipping
    threads_2 = []
    for m in range(len(multiplier)):
        scale = multiplier[m]
        imageToTest = cv2.resize(
            flipImg, (0, 0), fx=scale, fy=scale, interpolation=cv2.INTER_CUBIC)
        pad = [0,
                0,
                (imageToTest.shape[0] - model_params['stride']
                ) % model_params['stride'],
                (imageToTest.shape[1] - model_params['stride']
                ) % model_params['stride']
                ]

        imageToTest_padded = np.pad(imageToTest, ((0, pad[2]), (0, pad[3]), (
            0, 0)), mode='constant', constant_values=((0, 0), (0, 0), (0, 0)))
        input_img = imageToTest[np.newaxis, ...]

        data = json.dumps({"signature_name": "serving_default",
                    "instances": input_img.tolist()})
        # output_blobs = model.predict(input_img)

        logging.debug("Creating 2 body parts threads - second")
        thread_1 = threading.Thread(target=make_predictions, args=(data, "body_parts", "1"))
        thread_1.start()
        # thread_2 = threading.Thread(target=make_predictions, args=(data, "body_parts", "2"))
        # thread_2.start()
        threads_2.append(thread_1)

    logging.debug("Joining second set of body parts threads")

    for th in threads_2:
        th.join()

def run_testing(video_path):

    logging.info(f"Processing {video_path}")
    cap = cv2.VideoCapture(video_path)
    logging.info(f" Total frames - {int(cap.get(cv2.CAP_PROP_FRAME_COUNT))}")
    frame_num = 0
    while (cap.isOpened()):
        ret, frame = cap.read()
        if not ret:
            break
        logging.debug(frame.shape)
        logging.debug(f"Processing frame {frame_num}")
        # pipeline(frame)
        thread_1 = threading.Thread(target=bg_pipeline, args=(frame, ))
        logging.debug("Starting main bg thread")
        thread_1.start()
        thread_2 = threading.Thread(target=detect_umb, args=(frame, ))
        logging.debug("Starting main umb thread")
        thread_2.start()
        thread_3 = threading.Thread(target=body_parts_pipeline, args=(frame, ))
        logging.debug("Starting main Body parts thread")
        thread_3.start()

        logging.debug("Joining main bg thread")
        thread_1.join()
        logging.debug("Joining main umb thread")
        thread_2.join()
        logging.debug("Joining main body parts thread")
        thread_3.join()

        logging.debug(f"Done with frame pipeline {frame_num}")
        frame_num += 1

    logging.info("Done with Video")

if __name__ == "__main__":

    video_path = "serving/temp/input/iPhone11-Red_VideoFullRotation_v2.MOV"
    num_threads =  config['threads']
    threads = []
    logging.info(f"Total threads {num_threads}")
    for i in range(num_threads):
        x = threading.Thread(target=run_testing, args=(video_path, ))
        threads.append(x)
        x.start()
    print("Joining threads")
    for i in threads:
        i.join()

    logging.debug("--------------------------")
    