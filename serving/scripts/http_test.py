import logging
logging.basicConfig(level=logging.INFO)
import time
import requests
import json
import serving.scripts.bg_utils as bg_utils
import cv2
import tensorflow as tf
import numpy as np

host_address = "172.31.12.143"
port = "8503"

session = requests.Session()

headers = {"content-type": "application/json"}
def make_bg_predictions(data, host="1"):
    start_time = time.time()
    logging.info("Making Prediction")
    json_response = session.post(
            f'http://{host_address}:{port}/v1/models/bg_model:predict', data=data, headers=headers)
    result = json.loads(json_response.text)['predictions']
    logging.info(f"time for background subtraction - {time.time() - start_time}")
    logging.info(f"Done bg prediction prediction {host}")
    return result

def bg_pipeline(img):
    result =  None
    try:
        disp_image, image, h_pad, w_pad, resized = bg_utils.prepare_inputs(img, maintain_resolution=False)
        data = json.dumps({"signature_name": "serving_default",
                        "instances": image[None, ...].numpy().tolist()})
        result = make_bg_predictions(data)

        logging.debug("Done with bg pipeline")
    except Exception as e:
        logging.error(e)
        
    return result

if __name__ == "__main__":

    video_path = "/home/mohitm/repos/brarista_production/s3_buckets/br-cv-tech/DC_Datasets/Rotation_Videos/master/videos/61a0d99a54495581ec98b031/iPhone11-Red_VideoFullRotation_v1.MOV"
    cap = cv2.VideoCapture(video_path)
    frame_num = 1
    logging.debug("Processing video")
    try:
        while frame_num < 11:
            logging.debug(f"Processing frame {frame_num}")
            ret, frame = cap.read()
            # resize_shape = tuple(config["crop_size"])
            # frame = cv2.resize(frame, (1920, 1920))
            result = bg_pipeline(frame)
            cv2.imwrite(f"serving/logs/http_results/{frame_num}.png", np.array(result[0]) * 255)
            frame_num += 1
    except Exception as e:
        logging.error(f"Ye kya kiya {e}")


    # data = []
    # path = "/home/mohitm/repos/production_research/serving/configs/input/umb_detect.png"
    # path = "/home/mohitm/repos/brarista_production/temp/61215db30248013d2c5a8c64_cleavage.png"
    # image = cv2.imread(path)
    # image = cv2.resize(image, (1920, 1920))
    # # image = tf.convert_to_tensor(image[None])
    # image = image[None].astype(np.float32)
    # print(image.shape)

    
    # image = image[None]
    # print(image.shape)
    # image = tf.concat([image, image], axis = 0)
    # print(image.shape)