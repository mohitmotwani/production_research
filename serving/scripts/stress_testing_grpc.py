import os
import time
import cv2
import json
import threading
import logging
import numpy as np
import serving.scripts.bg_utils as bg_utils
import serving.scripts.umbilicus_utils as umb_utils
import serving.scripts.body_parts_utils as bp_utils
import requests
import argparse
import grpc
from tensorflow import reshape, make_tensor_proto, convert_to_tensor
import tensorflow as tf
from tensorflow_serving.apis import prediction_service_pb2_grpc
from tensorflow_serving.apis import predict_pb2

parser = argparse.ArgumentParser()
parser.add_argument('--row',
                    help='', required=False, default=1080)
parser.add_argument('--column',
                    help='', required=False, default=1080)
parser.add_argument("--log_file", required=False, default="serving/logs/stress_testing_grpc.log")

args = parser.parse_args()
print(args)
row = int(args.row)
column = int(args.column)
crop_size = (row, column)
log_file = args.log_file + f"_{row}"

MAX_GRPC_MESSAGE_LENGTH = 512 * 1920 * 1920


format = "[%(asctime)s] %(levelname)s {%(filename)s:%(lineno)d} - %(message)s"
logging.basicConfig(filename=log_file,level=logging.DEBUG, format=format, filemode='w+')

session = requests.Session()

headers = {"content-type": "application/json"}

os.environ["TF_CPP_MIN_LOG_LEVEL"] = "2"
os.environ['TF_FORCE_GPU_ALLOW_GROWTH'] = 'true'

with open("serving/configs/input/multi_serving_config.json", 'r') as f:
    config = json.load(f)

with open("serving/configs/input/body_parts_config.json", "r") as f:
    body_parts_config = json.load(f)

host_address = config[f'host_address_1']
port = config['port']
grpc_port = 8502

options = [
            ('grpc.max_send_message_length', MAX_GRPC_MESSAGE_LENGTH),
            ('grpc.max_receive_message_length', MAX_GRPC_MESSAGE_LENGTH)
]

channel = grpc.insecure_channel(f"{host_address}:{grpc_port}", options=options)
stub = prediction_service_pb2_grpc.PredictionServiceStub(channel)

# health_check_req = heal
# crop_size = config["crop_size"]

logging.info(f"Crop size is {crop_size}")

def make_raw_prediction(model_input, model_name, signature_name='serving_default', input_layer="input_tensor"):
    logging.info(f"Making {model_name} prediction")
    start_time = time.time()
    request = predict_pb2.PredictRequest()
    request.model_spec.name = model_name
    request.model_spec.signature_name = signature_name
    request.inputs[input_layer].CopyFrom(model_input)
    response = stub.Predict(request)
    logging.debug(
        f"Total Prediction time for {request.model_spec.name} {time.time() - start_time}")
    return response.outputs

def make_prediction(model_input, model_name, signature_name="serving_default", input_layer="input_1", output_layer="activation_49"):
    logging.info(f"Making {model_name} prediction")
    start_time = time.time()
    request = predict_pb2.PredictRequest()
    request.model_spec.name = model_name
    request.model_spec.signature_name = signature_name
    request.inputs[input_layer].CopyFrom(model_input)
    response = stub.Predict(request)
    predictions = response.outputs[list(response.outputs.keys())[0]]
    tensor_shape = predictions.tensor_shape
    vals = tuple([i.size for i in tensor_shape.dim])
    output = reshape(convert_to_tensor(predictions.float_val), (vals))
    logging.debug(
        f"Total grpc Prediction time for {request.model_spec.name} {time.time() - start_time}")
    return output

def bg_pipeline(img, alpha=0.7, maintain_resolution=False):
    try:
        disp_image, image, h_pad, w_pad, resized = bg_utils.prepare_inputs(img, maintain_resolution=False)
        pred = make_prediction(make_tensor_proto(image[None]), "bg_model")

        logging.debug("Done with bg pipeline")
    except Exception as e:
        logging.error(e)

def detect_umb(img, top_n=10):
    try:
        input_tensor = umb_utils.preprocess_img(img)

        output = make_raw_prediction(make_tensor_proto(input_tensor), "umbilicus")
    except Exception as e:
        logging.error(e)


def detect_body_parts(cvMat):
    try:
        params = body_parts_config['params']
        model_params = body_parts_config['model_params']
        # model = dnn_model['model']

        oriImg = cvMat.copy()
        flipImg = cv2.flip(oriImg, 1)
        oriImg = (oriImg / 256.0) - 0.5
        flipImg = (flipImg / 256.0) - 0.5
        multiplier = [x for x in params['scale_search']]

        segmap_scale = [None] * 8

        segmap_scale[3] = np.zeros(oriImg.shape[0] * oriImg.shape[1] * params["seg_num"]
                                    ).reshape(oriImg.shape[0], oriImg.shape[1], params["seg_num"])

        segmap_scale[7] = np.zeros(oriImg.shape[0] * oriImg.shape[1] * params["seg_num"]
                                    ).reshape(oriImg.shape[0], oriImg.shape[1], params["seg_num"])

        # tb._SYMBOLIC_SCOPE.value = True
        # bl._DISABLE_TRACKING.value = True

        for m in range(len(multiplier)):
            scale = multiplier[m]
            imageToTest = cv2.resize(
                oriImg, (0, 0), fx=scale, fy=scale, interpolation=cv2.INTER_CUBIC)

            pad = [0,
                    0,
                    (imageToTest.shape[0] - model_params['stride']
                    ) % model_params['stride'],
                    (imageToTest.shape[1] - model_params['stride']
                    ) % model_params['stride']
                    ]

            imageToTest_padded = np.pad(imageToTest, ((0, pad[2]), (0, pad[3]), (
                0, 0)), mode='constant', constant_values=((0, 0), (0, 0), (0, 0)))

            input_img = imageToTest[np.newaxis, ...].astype(np.float32)

            # TODO: tf serving
            output_blobs = make_prediction(make_tensor_proto(input_img), model_name = "body_parts")

            seg = np.squeeze(output_blobs[0])
            seg = cv2.resize(seg, (0, 0), fx=model_params['stride'], fy=model_params['stride'],
                            interpolation=cv2.INTER_CUBIC)
            seg = seg[:imageToTest_padded.shape[0] - pad[2],
                        :imageToTest_padded.shape[1] - pad[3], :]
            seg = cv2.resize(
                seg, (oriImg.shape[1], oriImg.shape[0]), interpolation=cv2.INTER_CUBIC)

            segmap_scale[m] = seg.copy()

        # flipping
        for m in range(len(multiplier)):
            scale = multiplier[m]
            imageToTest = cv2.resize(
                flipImg, (0, 0), fx=scale, fy=scale, interpolation=cv2.INTER_CUBIC)
            pad = [0,
                    0,
                    (imageToTest.shape[0] - model_params['stride']
                    ) % model_params['stride'],
                    (imageToTest.shape[1] - model_params['stride']
                    ) % model_params['stride']
                    ]

            imageToTest_padded = np.pad(imageToTest, ((0, pad[2]), (0, pad[3]), (
                0, 0)), mode='constant', constant_values=((0, 0), (0, 0), (0, 0)))
            input_img = imageToTest[np.newaxis, ...].astype(np.float32)

            # data = json.dumps({"signature_name": "serving_default",
            #             "instances": input_img.tolist()})
            # output_blobs = model.predict(input_img)
            output_blobs = make_prediction(make_tensor_proto(input_img), "body_parts")
            # extract outputs, resize, and remove padding

            seg = np.squeeze(output_blobs[0])
            seg = cv2.resize(seg, (0, 0), fx=model_params['stride'], fy=model_params['stride'],
                            interpolation=cv2.INTER_CUBIC)
            seg = seg[:imageToTest_padded.shape[0] - pad[2],
                        :imageToTest_padded.shape[1] - pad[3], :]
            seg = cv2.resize(
                seg, (oriImg.shape[1], oriImg.shape[0]), interpolation=cv2.INTER_CUBIC)

            seg_recover = bp_utils.recover_flipping_output(oriImg, seg, params)

            segmap_scale[4+m] = seg_recover.copy()

        segmap_a = np.maximum(segmap_scale[0], segmap_scale[1])
        segmap_b = np.maximum(segmap_scale[3], segmap_scale[2])
        segmap_c = np.maximum(segmap_scale[4], segmap_scale[5])
        segmap_d = np.maximum(segmap_scale[6], segmap_scale[7])
        seg_ori = np.maximum(segmap_a, segmap_b)
        seg_flip = np.maximum(segmap_c, segmap_d)

        return np.maximum(seg_ori, seg_flip)
    except Exception as e:
        logging.error(e)

def process_video(video_path):
    cap = cv2.VideoCapture(video_path)
    frame_num = 1
    logging.debug("Processing video")
    try:
        while (cap.isOpened()):
            logging.debug(f"Processing frame {frame_num}")
            ret, frame = cap.read()
            # resize_shape = tuple(config["crop_size"])
            frame = cv2.resize(frame, crop_size)
            if not ret:
                break
            threads = []
            
            thread_1 = threading.Thread(target=bg_pipeline, args=(frame,))
            threads.append(thread_1)
            thread_2 = threading.Thread(target=detect_umb, args=(frame,))
            threads.append(thread_2)
            thread_3 = threading.Thread(target=detect_body_parts, args=(frame, ))
            threads.append(thread_3)

            logging.debug("Starting all threads")

            for th in threads:
                th.start()

            logging.debug("JOining all threads")

            for th in threads:
                th.join()

            frame_num+=1
    except Exception as e:
        logging.error(e)

    logging.debug("Releasing Video")
    cap.release()

if __name__ == "__main__":
    video_1 = "s3_buckets/br-cv-tech/DC_Datasets/Rotation_Videos/master/videos/61a0d99a54495581ec98b031/iPhone11-Red_VideoFullRotation_v1.MOV"
    video_2 = "s3_buckets/br-cv-tech/DC_Datasets/Rotation_Videos/master/videos/61a0d99a54495581ec98b031/HuaweiP40-Lite_VideoFullRotation_v2.mp4"
    start_time = time.time()
    video_num = 1
    try:
        while True:
            for video in [video_1, video_2]:
                logging.info(f"Processing {video_num}")
                process_video(video)
                video_num += 1
            if time.time() - start_time > 86400:
                break
    
    except Exception as e:
        logging.error(e)
        logging.info(f"Total time {time.time() - start_time}")
        logging.info(f"Videos processed {video_num}")
        
    logging.info(f"Total time {time.time() - start_time}")
    logging.info(f"Videos processed {video_num}")