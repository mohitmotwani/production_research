import threading
import logging
import time
import cv2
import numpy as np
import time
import grpc
from grpc.beta import implementations
# import tensorflow.compat.v1 as tf
import tensorflow as tf
import os
import serving.scripts.bg_utils as bg_utils

from tensorflow.compat.v1 import make_tensor_proto
MAX_GRPC_MESSAGE_LENGTH = 512 * 1024 * 1024

if os.environ.get('https_proxy'):
    del os.environ['https_proxy']
if os.environ.get('http_proxy'):
    del os.environ['http_proxy']
# grp


from tensorflow_serving.apis import predict_pb2
from tensorflow_serving.apis import prediction_service_pb2_grpc
from tensorflow_serving.apis import prediction_service_pb2, get_model_metadata_pb2

logging.basicConfig(level=logging.INFO)

# tf.app.flags.DEFINE_string(
#     "server", "172.31.12.143:8503", "PredictionService host:port")
# # tf.app.flags.DEFINE_string("image", "", "path to image in JPEG format")
# FLAGS = tf.app.flags.FLAGS
options = [
        ('grpc.max_send_message_length', MAX_GRPC_MESSAGE_LENGTH),
        ('grpc.max_receive_message_length', MAX_GRPC_MESSAGE_LENGTH)
    ]


def get_stub(host, port):
    channel = grpc.insecure_channel(f"{host}:{port}", options=options)
    stub = prediction_service_pb2_grpc.PredictionServiceStub(channel)
    return stub

def get_model_prediction(model_input, stub, model_name="bg_model", signature_name='serving_default'):
    start_time =  time.time()
    request = predict_pb2.PredictRequest()
    request.model_spec.name = model_name
    request.model_spec.signature_name = signature_name
    # request.inputs['input_1'].CopyFrom(tf.make_tensor_proto(model_input, shape = model_input.shape))
    request.inputs['input_1'].CopyFrom(tf.make_tensor_proto(model_input))
    # response = stub.Predict.future(request, 5.0)  # 5 seconds
    response = stub.Predict(request)  # 5 seconds
    print(f"Total Time - {time.time() - start_time}")
    # return response
    # result = respon.outputs['activation_49']
    return tf.reshape(tf.convert_to_tensor(response.outputs['activation_49'].float_val), (1920, 1920)).numpy()

def get_model_version(model_name, stub):
    request = get_model_metadata_pb2.GetModelMetadataRequest()
    request.model_spec.name = model_name
    request.model_spec.signature_name = "serving_default"
    request.metadata_field.append("signature_def")
    response = stub.GetModelMetadata(request)
    # signature of loaded model is available here: response.metadata['signature_def']
    return response.model_spec.version.value

if __name__ == "__main__":


    data = []
    path = "/home/mohitm/repos/production_research/serving/configs/input/umb_detect.png"
    path = "/home/mohitm/repos/brarista_production/temp/61215db30248013d2c5a8c64_cleavage.png"
    image = cv2.imread(path)
    # image = cv2.resize(image, (1920, 1920))
    # # image = tf.convert_to_tensor(image[None])
    # image = image[None].astype(np.float32)
    # print(image.shape)

    disp_image, image, h_pad, w_pad, resized = bg_utils.prepare_inputs(image, maintain_resolution=False)
    image = image[None]
    print(image.shape)
    image = tf.concat([image, image], axis = 0)
    print(image.shape)
    # with open(path, 'rb') as f:
    #     image = f.read()
    # for i in range(1):
    #         data.append(image)


    stub = get_stub("172.31.12.143", port = 8503)
    print(get_model_version("bg_model", stub))
    r = get_model_prediction(image, stub)
    cv2.imwrite("test_2.png", r*255)



# export GRPC_TRACE=all
# export GRPC_VERBOSITY=debug