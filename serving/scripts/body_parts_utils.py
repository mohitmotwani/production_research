import numpy as np
import cv2 as cv
import requests
import json
import os

host_address = "172.31.14.37"
port = 8503
model_name = "body_parts"
headers = {"content-type": "application/json"}

body_parts_config = {
    "model_params" :{
            "boxsize" : 368,
            "padValue" : 128,
            "stride" : 8,
            "caffemodel" : "./model/_trained_COCO/pose_iter_440000.caffemodel",
            "deployFile" : "./model/_trained_COCO/pose_deploy.prototxt",
            "description" : "COCO Pose56 Two-level Linevec",
            "np" : 12,
            "part_str" : ["nose", "neck", "Rsho", "Relb", "Rwri", "Lsho", "Lelb", "Lwri", "Rhip", "Rkne", "Rank", "Lhip", "Lkne", "Lank", "Leye", "Reye", "Lear", "Rear", "pt19"]
        },
    "params" : {
            "use_gpu" : 1,
            "GPUdeviceNumber" : 0,
            "modelID" : 1,
            "octave" : 3,
            "starting_range" : 0.8,
            "ending_range" : 2,
            "scale_search" : [0.3, 0.5, 1],
            "thre1" : 0.1,
            "thre2" : 0.05,
            "thre3" : 0.5,
            "min_num" : 4,
            "mid_num" : 10,
            "crop_ratio" : 2.5,
            "bbox_ratio" : 0.25,
            "human_part" : [0,1,2,3,4,5,6],
            "human_part_thresh" : [0.6, 0.5, 0.8, 0.55, 0.55, 0.55, 0.55],
            "human_part_name": ["background", "head", "torso", "left upper arm", "right upper arm", "left forearm", "right forearm"],
            "human_ori_part" : [0,1,2,3,4,5,6],
            "seg_num" : 7
        }

}

def recover_flipping_output(oriImg, part_ori_size, params):
        part_ori_size = part_ori_size[:, ::-1, :]
        part_flip_size = np.zeros(
            (oriImg.shape[0], oriImg.shape[1], params["seg_num"]))
        part_flip_size[:, :, params["human_ori_part"]
                       ] = part_ori_size[:, :, params["human_ori_part"]]
        return part_flip_size

def remove_segment_noise(image, idx):
        image[image != idx] = 0
        image_proc = image.copy()
        image_proc[image_proc == idx] = 255
        image_proc = np.uint8(image_proc)
        image_refine = np.zeros((image.shape))
        areaArray = []
        contours, _ = cv.findContours(
            image_proc, cv.RETR_EXTERNAL, cv.CHAIN_APPROX_SIMPLE)
        for i, c in enumerate(contours):
            area = cv.contourArea(c)
            areaArray.append(area)
        sorteddata = sorted(zip(areaArray, contours),
                            key=lambda x: x[0], reverse=True)
        if idx == 1:    # Torso Part
            if len(sorteddata) > 0:
                firstlargestcontour = sorteddata[0][1]
                x, y, w, h = cv.boundingRect(firstlargestcontour)
                image_refine[y:y+h, x:x+w] = image[y:y+h, x:x+w]
        if idx == 2:    # Torso Part
            if len(sorteddata) > 0:
                firstlargestcontour = sorteddata[0][1]
                x, y, w, h = cv.boundingRect(firstlargestcontour)
                image_refine[y:y+h, x:x+w] = image[y:y+h, x:x+w]
        elif idx == 3:  # Hand Part
            if len(sorteddata) > 1:
                firstlargestcontour = sorteddata[0][1]
                x, y, w, h = cv.boundingRect(firstlargestcontour)
                image_refine[y:y+h, x:x+w] = image[y:y+h, x:x+w]
                secondlargestcontour = sorteddata[1][1]
                x, y, w, h = cv.boundingRect(secondlargestcontour)
                image_refine[y:y+h, x:x+w] = image[y:y+h, x:x+w]
        kernel = np.ones((15, 15), np.uint8)
        # Image closing operation
        image_refine = cv.dilate(image_refine, kernel)
        image_refine = cv.erode(image_refine, kernel)
        image_refine[image_refine > 0] = 255
        return image_refine

def detect_first_stage_body_parts(cvMat):
        params = body_parts_config['params']
        model_params = body_parts_config['model_params']
        # model = dnn_model['model']

        oriImg = cvMat.copy()
        flipImg = cv.flip(oriImg, 1)
        oriImg = (oriImg / 256.0) - 0.5
        flipImg = (flipImg / 256.0) - 0.5
        multiplier = [x for x in params['scale_search']]

        segmap_scale = [None] * 8

        segmap_scale[3] = np.zeros(oriImg.shape[0] * oriImg.shape[1] * params["seg_num"]
                                   ).reshape(oriImg.shape[0], oriImg.shape[1], params["seg_num"])

        segmap_scale[7] = np.zeros(oriImg.shape[0] * oriImg.shape[1] * params["seg_num"]
                                   ).reshape(oriImg.shape[0], oriImg.shape[1], params["seg_num"])

        # tb._SYMBOLIC_SCOPE.value = True
        # bl._DISABLE_TRACKING.value = True

        for m in range(len(multiplier)):
            scale = multiplier[m]
            imageToTest = cv.resize(
                oriImg, (0, 0), fx=scale, fy=scale, interpolation=cv.INTER_CUBIC)

            pad = [0,
                   0,
                   (imageToTest.shape[0] - model_params['stride']
                    ) % model_params['stride'],
                   (imageToTest.shape[1] - model_params['stride']
                    ) % model_params['stride']
                   ]

            imageToTest_padded = np.pad(imageToTest, ((0, pad[2]), (0, pad[3]), (
                0, 0)), mode='constant', constant_values=((0, 0), (0, 0), (0, 0)))

            input_img = imageToTest[np.newaxis, ...]

            # TODO: tf serving
            data = json.dumps({"signature_name": "serving_default",
                      "instances": input_img.tolist()})
            # output_blobs = model.predict(input_img)
            json_response = requests.post(
                f'http://{host_address}:{port}/v1/models/{model_name}:predict', data=data, headers=headers)
            output_blobs = json.loads(json_response.text)['predictions']

            seg = np.squeeze(output_blobs[0])
            seg = cv.resize(seg, (0, 0), fx=model_params['stride'], fy=model_params['stride'],
                            interpolation=cv.INTER_CUBIC)
            seg = seg[:imageToTest_padded.shape[0] - pad[2],
                      :imageToTest_padded.shape[1] - pad[3], :]
            seg = cv.resize(
                seg, (oriImg.shape[1], oriImg.shape[0]), interpolation=cv.INTER_CUBIC)

            segmap_scale[m] = seg.copy()

        # flipping
        for m in range(len(multiplier)):
            scale = multiplier[m]
            imageToTest = cv.resize(
                flipImg, (0, 0), fx=scale, fy=scale, interpolation=cv.INTER_CUBIC)
            pad = [0,
                   0,
                   (imageToTest.shape[0] - model_params['stride']
                    ) % model_params['stride'],
                   (imageToTest.shape[1] - model_params['stride']
                    ) % model_params['stride']
                   ]

            imageToTest_padded = np.pad(imageToTest, ((0, pad[2]), (0, pad[3]), (
                0, 0)), mode='constant', constant_values=((0, 0), (0, 0), (0, 0)))
            input_img = imageToTest[np.newaxis, ...]

            data = json.dumps({"signature_name": "serving_default",
                      "instances": input_img.tolist()})
            # output_blobs = model.predict(input_img)
            json_response = requests.post(
                f'http://{host_address}:{port}/v1/models/{model_name}:predict', data=data, headers=headers)
            output_blobs = json.loads(json_response.text)['predictions']
            # extract outputs, resize, and remove padding

            seg = np.squeeze(output_blobs[0])
            seg = cv.resize(seg, (0, 0), fx=model_params['stride'], fy=model_params['stride'],
                            interpolation=cv.INTER_CUBIC)
            seg = seg[:imageToTest_padded.shape[0] - pad[2],
                      :imageToTest_padded.shape[1] - pad[3], :]
            seg = cv.resize(
                seg, (oriImg.shape[1], oriImg.shape[0]), interpolation=cv.INTER_CUBIC)

            seg_recover = recover_flipping_output(oriImg, seg, params)

            segmap_scale[4+m] = seg_recover.copy()

        segmap_a = np.maximum(segmap_scale[0], segmap_scale[1])
        segmap_b = np.maximum(segmap_scale[3], segmap_scale[2])
        segmap_c = np.maximum(segmap_scale[4], segmap_scale[5])
        segmap_d = np.maximum(segmap_scale[6], segmap_scale[7])
        seg_ori = np.maximum(segmap_a, segmap_b)
        seg_flip = np.maximum(segmap_c, segmap_d)

        return np.maximum(seg_ori, seg_flip)


def TorsoDetection(cvMat):
        seg_avg = detect_first_stage_body_parts(cvMat)
        # 2. Refinement
        seg_avg_argmax = np.argmax(seg_avg, axis=-1)
        seg_avg_max = np.max(seg_avg, axis=-1)
        # 3. Part Thresholding -- Applying detected threshold on the detected body parts
        # thresh_mask = self.part_threshold(seg_avg_argmax, dnn_model["params"])
        seg_max_thresh = (
            seg_avg_max > body_parts_config["params"]["thre1"]).astype(np.uint8)
        seg_avg_argmax *= seg_max_thresh
        seg_result = seg_avg_argmax.copy()
        # seg_full = seg_result.copy()
        # 4. Extracting only Upper hands and Torso area
        #seg_result[np.logical_and(seg_result[:,:]!=2,seg_result[:,:]!=3,seg_result[:,:]!=1)] = 0
        # 5. Removing Noisy Torso signals
        head_clean = remove_segment_noise(seg_result.copy(), 1)
        torso_clean = remove_segment_noise(seg_result.copy(), 2)
        hands_clean = remove_segment_noise(seg_result.copy(), 3)
        return (torso_clean, head_clean, hands_clean, seg_result)

if __name__ == "__main__":
    torso, head, hands, seg = TorsoDetection(cv.imread("serving/temp/test.png"))