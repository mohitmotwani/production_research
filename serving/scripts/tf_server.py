import time
import logging
import multiprocessing
import threading
import tensorflow as tf
import cv2
import random
import requests
import json
import numpy as np
import os
import utils
import nvidia_smi

random.seed(40)
np.random.seed(40)

nvidia_smi.nvmlInit()
handle = nvidia_smi.nvmlDeviceGetHandleByIndex(0)

os.environ["TF_CPP_MIN_LOG_LEVEL"] = "2"

os.environ['TF_FORCE_GPU_ALLOW_GROWTH'] = 'true'

with open("serving/configs/input/serving_config.json", 'r') as f:
    config = json.load(f)

num_threads = config['threads']
host_address = config['host_address']
port = config['port']
crop = config["crop_size"]
requests_per_thread = config["requests_per_thread"]
exp_num = config["exp_num"]
requests_made = 0

experiment_dict = {}
experiment_dict['total_threads'] = num_threads
experiment_dict['batching']=True
experiment_dict['thread_times'] = {}
experiment_dict["request_times"] = {}
experiment_dict["request_memory"] = {}


headers = {"content-type": "application/json"}
logging_format = "[%(asctime)s] %(levelname)s - %(threadName)s %(message)s"
logging.basicConfig(filename=f"serving/temp/logs/serving_log_{port}.log",
                    level=logging.DEBUG,
                    format=logging_format,
                    datefmt="%d-%m-%Y %H:%M:%S")


def resize_mask(mask, size):
    with tf.device('/device:cpu:0'):
        mask = tf.image.resize(mask[..., None], size, method='nearest')
        return mask[..., 0]


def resize_preserve_aspect_ratio(image_tensor, max_side_dim):
    with tf.device('/device:cpu:0'):
        img_h, img_w = image_tensor.shape.as_list()[:2]
        min_dim = tf.maximum(img_h, img_w)
        resize_ratio = max_side_dim / min_dim
        new_h, new_w = resize_ratio * img_h, resize_ratio * img_w
        resized_image_tensor = tf.image.resize(
            image_tensor, size=[new_h, new_w], method='bilinear')
        return resized_image_tensor


def pad_inputs(image, pad_value=0):
    with tf.device('/device:cpu:0'):
        dims = tf.cast(tf.shape(image), dtype=tf.float32)
        h_pad = tf.maximum(crop[0] - dims[0], 0)
        w_pad = tf.maximum(crop[1] - dims[1], 0)
        padded_image = tf.pad(
            image,
            paddings=[[0, h_pad], [0, w_pad], [0, 0]],
            constant_values=pad_value)
        return padded_image, h_pad, w_pad


def prepare_inputs(img, maintain_resolution=False):
    # Convert img to RGB
    with tf.device('/device:cpu:0'):
        rgb = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
        # crop = [1920, 1920]
        # Is optional but i recommend (float convertion and convert img to tensor image)

        image = tf.convert_to_tensor(rgb)

        # image = tf.io.read_file(img)
        # image = tf.image.decode_image(image, channels=3)
        image.set_shape([None, None, 3])
        shape = image.shape.as_list()[:2]

        if maintain_resolution:
            disp_image = image.numpy().copy()
        image = tf.cast(image, dtype=tf.float32)
        resized = False
        if tf.maximum(shape[0], shape[1]) > crop[0]:
            resized = True
            image = resize_preserve_aspect_ratio(image, max_side_dim=crop[0])
        image, h_pad, w_pad = pad_inputs(image)
        if not maintain_resolution:
            disp_image = image.numpy().copy()
        image = image[:, :, ::-1] - tf.constant([
            103.939,
            116.779,
            123.68
        ])
        return disp_image, tf.cast(image, dtype=tf.float32), np.int32(h_pad.numpy()), np.int32(w_pad.numpy()), resized


def pipeline(img, alpha=0.7, maintain_resolution=False):
    global requests_made, host_address, port, experiment_dict

    nvidia_smi.nvmlInit()
    handle = nvidia_smi.nvmlDeviceGetHandleByIndex(0)
    info = nvidia_smi.nvmlDeviceGetMemoryInfo(handle)
    disp_image, image, h_pad, w_pad, resized = prepare_inputs(
        img, maintain_resolution=False)
# requests.post("http://loclahost:8501/v1/models/")
    data = json.dumps({"signature_name": "serving_default",
                      "instances": image[None, ...].numpy().tolist()})
    model_names = ["first_model", "second_model", "third_model",
                   "fourth_model", "fifth_model", "sixth_model"]
    model_names = ["first_model"]
    for i in range(requests_per_thread):
        logging.debug(f"Run_{i}")
        for model_name in model_names:
            start_time = time.time()
            info = nvidia_smi.nvmlDeviceGetMemoryInfo(handle)
            experiment_dict["request_memory"][f"request_{requests_made}_before"] = utils.bytes_to_gb(info.used)
            json_response = requests.post(
                f'http://{host_address}:{port}/v1/models/{model_name}:predict', data=data, headers=headers)
            pred = json.loads(json_response.text)['predictions']
            requests_made += 1
            experiment_dict["request_times"][f"request_{requests_made}"] = time.time() - start_time
            info = nvidia_smi.nvmlDeviceGetMemoryInfo(handle)
            experiment_dict["request_memory"][f"request_{requests_made}_after"] = utils.bytes_to_gb(info.used)
            logging.debug(f"{model_name}")
            tf.keras.backend.clear_session() #@IgnoreException

    logging.debug(json_response.text[:1000])
    # return None
    with tf.device('/device:cpu:0'):
        pred = tf.convert_to_tensor(pred)
        mask = pred[0, ..., 0] > 0.5
        mask = tf.cast(mask, dtype=tf.uint8)
        b_h, b_w = (image.shape[:2] - tf.constant([h_pad, w_pad])).numpy()
        disp_mask = mask[:b_h, :b_w].numpy()
        if resized and maintain_resolution:
            disp_mask = resize_mask(disp_mask, disp_image.shape[:2]).numpy()
        else:
            disp_image = disp_image[:b_h, :b_w]
        res = disp_mask.copy()
        res[disp_mask > 0] = 255
        experiment_dict['thread_times'][str(threading.get_ident()) + "_time"] = time.thread_time()
        return np.uint8(res)


def run_tensorflow(img, num_threads=1):
    threads = []
    logging.info(f"Total threads {num_threads}")
    for i in range(num_threads):
        x = threading.Thread(target=pipeline, args=(img, ))
        threads.append(x)
        x.start()
    print("Joining threads")
    for i in threads:
        i.join()

def compute_experiment_stats():
    global experiment_dict
    experiment_dict["average_thread_time"] = np.mean(list(experiment_dict['thread_times'].values()))
    experiment_dict["average_request_time"] = np.mean(list(experiment_dict['request_times'].values()))
    experiment_dict["average_request_memory"] = np.mean(list(experiment_dict['request_memory'].values()))
    experiment_dict["total_request"] = len(experiment_dict['request_times'].keys())
    pass


if __name__ == "__main__":
    s3_path = "s3_buckets/br-cv-tech"
    dc_dataset = os.path.join(s3_path, "DC_Datasets")
    card_images = os.path.join(
        dc_dataset, "Card_Alpha_Pipeline", "master", "images")
    user_id_card_images = [os.path.join(
        card_images, user, 'HuaweiP40-Lite_Card90Degree1.jpg') for user in os.listdir(card_images)]

    u_id = random.randint(0, len(user_id_card_images))
    logging.info(u_id)
    logging.debug(user_id_card_images[u_id])
    img = cv2.imread(user_id_card_images[u_id])
    logging.debug(img.shape)
    start_time = time.time()
    info = nvidia_smi.nvmlDeviceGetMemoryInfo(handle)
    experiment_dict["memory_before_process_start"] = utils.bytes_to_gb(info.used)
    logging.debug(f"Memory used {info.used}")
    threads = []
    logging.info(f"Total threads {num_threads}")
    for i in range(num_threads):
        x = threading.Thread(target=pipeline, args=(img, ))
        threads.append(x)
        x.start()
    print("Joining threads")
    for i in threads:
        i.join()
    # logging.info("Starting Process")
    # p.start()
    # logging.info("Joining Process")
    # p.join()
    info = nvidia_smi.nvmlDeviceGetMemoryInfo(handle)
    experiment_dict["memory_after_process_start"] = utils.bytes_to_gb(info.used)
    experiment_dict["Total Time"] = time.time() - start_time
    logging.info(f"Total time {time.time() - start_time}")
    compute_experiment_stats()
    experiment_dict['image_shape'] = img.shape

    logging.debug("--------------------------")

    # with open(f"serving/temp/serving_logs/experiment_{exp_num}.json", 'w') as f:
    #     json.dump(experiment_dict, f, indent=4)

    # with open("serving/configs/input/serving_config.json", 'w') as f:
    #     config['exp_num'] = exp_num + 1
    #     json.dump(config, f, indent=4)


