import cv2
import tensorflow as tf
import numpy as np

crop = [1920, 1920]

def resize_mask(mask, size):
    with tf.device('/device:cpu:0'):
        mask = tf.image.resize(mask[..., None], size, method='nearest')
        return mask[..., 0]


def resize_preserve_aspect_ratio(image_tensor, max_side_dim):
    with tf.device('/device:cpu:0'):
        img_h, img_w = image_tensor.shape.as_list()[:2]
        min_dim = tf.maximum(img_h, img_w)
        resize_ratio = max_side_dim / min_dim
        new_h, new_w = resize_ratio * img_h, resize_ratio * img_w
        resized_image_tensor = tf.image.resize(
            image_tensor, size=[new_h, new_w], method='bilinear')
        return resized_image_tensor

def pad_inputs(image, pad_value=0):
    with tf.device('/device:cpu:0'):
        dims = tf.cast(tf.shape(image), dtype=tf.float32)
        h_pad = tf.maximum(crop[0] - dims[0], 0)
        w_pad = tf.maximum(crop[1] - dims[1], 0)
        padded_image = tf.pad(
            image,
            paddings=[[0, h_pad], [0, w_pad], [0, 0]],
            constant_values=pad_value)
        return padded_image, h_pad, w_pad

def prepare_inputs(img, maintain_resolution=False):
    # Convert img to RGB
    with tf.device('/device:cpu:0'):
        rgb = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
        # crop = [1920, 1920]
        # Is optional but i recommend (float convertion and convert img to tensor image)

        image = tf.convert_to_tensor(rgb)

        # image = tf.io.read_file(img)
        # image = tf.image.decode_image(image, channels=3)
        image.set_shape([None, None, 3])
        shape = image.shape.as_list()[:2]

        if maintain_resolution:
            disp_image = image.numpy().copy()
        image = tf.cast(image, dtype=tf.float32)
        resized = False
        if tf.maximum(shape[0], shape[1]) > crop[0]:
            resized = True
            image = resize_preserve_aspect_ratio(image, max_side_dim=crop[0])
        image, h_pad, w_pad = pad_inputs(image)
        if not maintain_resolution:
            disp_image = image.numpy().copy()
        image = image[:, :, ::-1] - tf.constant([
            103.939,
            116.779,
            123.68
        ])
        return disp_image, tf.cast(image, dtype=tf.float32), np.int32(h_pad.numpy()), np.int32(w_pad.numpy()), resized


def post_process_predictions(pred):
    with tf.device('/device:cpu:0'):
        pred = tf.convert_to_tensor(pred)
        mask = pred[0, ..., 0] > 0.5
        mask = tf.cast(mask, dtype=tf.uint8)
        b_h, b_w = (image.shape[:2] - tf.constant([h_pad, w_pad])).numpy()
        disp_mask = mask[:b_h, :b_w].numpy()
        if resized and maintain_resolution:
            disp_mask = resize_mask(disp_mask, disp_image.shape[:2]).numpy()
        else:
            disp_image = disp_image[:b_h, :b_w]
        res = disp_mask.copy()
        res[disp_mask > 0] = 255
        return np.uint8(res)

    