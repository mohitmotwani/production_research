# Commands
### Serving six models with Batching

```
sudo docker run -d --gpus all \
-p 8503:8501 \
--mount type=bind,source=/home/mohitm/repos/production_research/serving/models/bg_subtract,target=/models/first_model \
--mount type=bind,source=/home/mohitm/repos/production_research/serving/models/bg_subtract,target=/models/second_model \
--mount type=bind,source=/home/mohitm/repos/production_research/serving/models/bg_subtract,target=/models/third_model \
--mount type=bind,source=/home/mohitm/repos/production_research/serving/models/bg_subtract,target=/models/fourth_model \
--mount type=bind,source=/home/mohitm/repos/production_research/serving/models/bg_subtract,target=/models/fifth_model \
--mount type=bind,source=/home/mohitm/repos/production_research/serving/models/bg_subtract,target=/models/sixth_model \
--mount type=bind,source=/home/mohitm/repos/production_research/serving/configs/input/tf_config.config,target=/models/tf_config.config \
--mount type=bind,source=/home/mohitm/repos/production_research/serving/configs/input/batching.config,target=/models/batching.config \
-t tensorflow/serving:latest-gpu \
--model_config_file=/models/tf_config.config \
--batching_parameters_file=/models/batching.config \
--enable_batching \
--concurrency=10 \
> serving/temp/logs/server_gpu_2.log
```

### Serving six models without batching
```
sudo docker run -d --gpus all \
-p 8503:8501 \
--mount type=bind,source=/home/mohitm/repos/production_research/serving/models/bg_subtract,target=/models/first_model \
--mount type=bind,source=/home/mohitm/repos/production_research/serving/models/bg_subtract,target=/models/second_model \
--mount type=bind,source=/home/mohitm/repos/production_research/serving/models/bg_subtract,target=/models/third_model \
--mount type=bind,source=/home/mohitm/repos/production_research/serving/models/bg_subtract,target=/models/fourth_model \
--mount type=bind,source=/home/mohitm/repos/production_research/serving/models/bg_subtract,target=/models/fifth_model \
--mount type=bind,source=/home/mohitm/repos/production_research/serving/models/bg_subtract,target=/models/sixth_model \
--mount type=bind,source=/home/mohitm/repos/production_research/serving/configs/input/tf_config.config,target=/models/tf_config.config \
-t tensorflow/serving:latest-gpu \
--model_config_file=/models/tf_config.config \
> serving/temp/logs/server_gpu_2.log
```

## One Model

```
sudo docker run -d --gpus all \
-p 8503:8501 \
--mount type=bind,source=/home/mohitm/repos/production_research/serving/models/umbilicus,target=/models/umbilicus \
--mount type=bind,source=/home/mohitm/repos/production_research/serving/configs/input/umbilicus.config,target=/models/umbilicus.config \
-t tensorflow/serving:latest-gpu \
--model_config_file=/models/umbilicus.config \
```


```
# TF Serving

```bash
sudo docker run -t --rm -p 8501:8501 -v "/home/mohitm/repos/brarista_system_testing/models/bg_subtract:/models/bg_subtract" -e MODEL_NAME=bg_subtract tensorflow/serving > temp/server.log
```

## GPU

```bash
sudo docker run --gpus all -dt --rm -p 8501:8501 -v "/home/mohitm/repos/brarista_system_testing/models/bg_subtract:/models/bg_subtract" -e MODEL_NAME=bg_subtract tensorflow/serving:latest-gpu > temp/server_gpu.log
```

or

```bash
sudo docker run --gpus all -p 8501:8501 --mount type=bind,source=/home/mohitm/repos/brarista_system_testing/models/bg_subtract,target=/models/bg_subtract -e MODEL_NAME=bg_subtract -t tensorflow/serving:latest-gpu > temp/server_gpu.log &
```

[https://github.com/NVIDIA/nvidia-docker/issues/1238](https://github.com/NVIDIA/nvidia-docker/issues/1238)

### Installing nvidia-container-toolkit

For tf serving to be able to run on GPU, need to install nvidia-container-toolkit. if `sudo apt-get update and sudo apt-get install nvidia-container-toolkit` doesn’t work use:

```bash
distribution=$(. /etc/os-release;echo $ID$VERSION_ID)
curl -s -L https://nvidia.github.io/nvidia-docker/gpgkey | sudo apt-key add -
curl -s -L https://nvidia.github.io/nvidia-docker/$distribution/nvidia-docker.list | sudo tee /etc/apt/sources.list.d/nvidia-docker.list

sudo apt-get update && sudo apt-get install -y nvidia-container-toolkit
sudo systemctl restart docker
```

### When GPU Serving doesn’t work

<aside>
💡 This worked - os.environ['TF_FORCE_GPU_ALLOW_GROWTH'] = 'true’ # for gpu

</aside>

[Failed to get convolution algorithm. This is probably because cuDNN failed to initialize,](https://stackoverflow.com/questions/53698035/failed-to-get-convolution-algorithm-this-is-probably-because-cudnn-failed-to-in)

[https://github.com/tensorflow/tensorflow/issues/24496](https://github.com/tensorflow/tensorflow/issues/24496)

## Running 3 models

```sudo docker run -d --gpus all -p 8503:8501 --mount type=bind,source=/home/mohitm/repos/production_research/serving/models/bg_subtract,target=/models/first_model \
--mount type=bind,source=/home/mohitm/repos/production_research/serving/models/bg_subtract,target=/models/second_model \
--mount type=bind,source=/home/mohitm/repos/production_research/serving/models/bg_subtract,target=/models/third_model \
--mount type=bind,source=/home/mohitm/repos/production_research/serving/configs/input/tf_config_2.config,target=/models/tf_config.config \
-t tensorflow/serving:latest-gpu \
--model_config_file=/models/tf_config.config

```

## Running 3 models with batching

```
sudo docker run -d --gpus all -p 8503:8501 \
--mount type=bind,source=/home/mohitm/repos/production_research/serving/models/bg_subtract,target=/models/bg_model \
--mount type=bind,source=/home/mohitm/repos/production_research/serving/models/umbilicus,target=/models/umbilicus \
--mount type=bind,source=/home/mohitm/repos/production_research/serving/models/bg_subtract,target=/models/third_model \
--mount type=bind,source=/home/mohitm/repos/production_research/serving/configs/input/tf_config_2.config,target=/models/tf_config.config \
--mount type=bind,source=/home/mohitm/repos/production_research/serving/configs/input/batching.config,target=/models/batching.config \
-t tensorflow/serving:latest-gpu \
--model_config_file=/models/tf_config.config \
--batching_parameters_file=/models/batching.config \
--enable_batching \
--concurrency=10 \
```

## Running 3 models without batching

```
sudo docker run -d --gpus all -p 8503:8501 \
--mount type=bind,source=/serving/models/bg_subtract,target=/models/bg_model \
--mount type=bind,source=/serving/models/umbilicus,target=/models/umbilicus \
--mount type=bind,source=/serving/models/bg_subtract,target=/models/third_model \
--mount type=bind,source=/serving/configs/input/tf_serving.config,target=/models/tf_config.config \
-t tensorflow/serving:latest-gpu \
--model_config_file=/models/tf_config.config \
```



### To limit GPU Memory add this flag

```
--per_process_gpu_memory_fraction=0.7 
```

## Single Model Batching
```
sudo docker run -d --gpus all \
-p 8503:8501 \
--mount type=bind,source=/home/mohitm/repos/production_research/serving/models/bg_subtract,target=/models/first_model \
--mount type=bind,source=/home/mohitm/repos/production_research/serving/configs/input/tf_config_2.config,target=/models/tf_config.config \
--mount type=bind,source=/home/mohitm/repos/production_research/serving/configs/input/single_model_batching.config,target=/models/batching.config \
-t tensorflow/serving:latest-gpu \
--model_config_file=/models/tf_config.config \
--batching_parameters_file=/models/batching.config \
--enable_batching \
--concurrency=10 \
--per_process_gpu_memory_fraction=0.6
```

# Notes

- If the tf_config file refers to models that are not mounted in the container, then the docker will not run. So make sure that the models in config and the models that you mount are the same


# Important Links

## [The docker command parameters and their explanations](https://github.com/tensorflow/serving/issues/249#issuecomment-425011436)

[Tensorflow Serving number of requests in queue](https://stackoverflow.com/questions/57256298/tensorflow-serving-number-of-requests-in-queue)



sudo docker run -d --gpus all -p 8503:8500 \
--mount type=bind,source=/serving/models/bg_subtract,target=/models/bg_model \
--mount type=bind,source=/serving/models/umbilicus,target=/models/umbilicus \
--mount type=bind,source=/serving/models/body_parts,target=/models/body_parts \
--mount type=bind,source=/serving/configs/tf_serving.config,target=/models/tf_config.config \
-t tensorflow/serving:latest-gpu \
--model_config_file=/models/tf_config.config --enable_batching


## Current Docker Statement

sudo docker run -d --gpus all -p 8502:8500 -p 8503:8501 \
--mount type=bind,source=/serving/models/bg_subtract,target=/models/bg_model \
--mount type=bind,source=/serving/models/umbilicus,target=/models/umbilicus \
--mount type=bind,source=/serving/models/body_parts,target=/models/body_parts \
--mount type=bind,source=/serving/configs/tf_serving.config,target=/models/tf_config.config \
-t tensorflow/serving:latest-gpu \
--model_config_file=/models/tf_config.config \
--model_config_file_poll_wait_seconds=60 \
--grpc_max_threads 100  

## restart automatically

```
docker update --restart unless-stopped $(sudo docker ps -q)
```

## Run Script
```
sudo docker run  -d --gpus all -p 8502:8500 -p 8503:8501 --name tf_serving_docker --mount type=bind,source=/serving/models/bg_subtract,target=/models/bg_model --mount type=bind,source=/serving/models/umbilicus,target=/models/umbilicus --mount type=bind,source=/serving/models/body_parts,target=/models/body_parts --mount type=bind,source=/serving/configs/tf_serving.config,target=/models/tf_config.config --mount type=bind,source=/serving/configs/platform_config_file.config,target=/models/platform_config_file.config -t tensorflow/serving:latest-gpu --model_config_file=/models/tf_config.config --model_config_file_poll_wait_seconds=60 --grpc_max_threads 100 --platform_config_file=/models/platform_config_file.config

sudo docker update --restart unless-stopped tf_serving_docker

```

