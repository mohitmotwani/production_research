To run this script:

1. You need nvidia-container-toolkit to be installed. Run the following in your bash terminal
```
distribution=$(. /etc/os-release;echo $ID$VERSION_ID)
curl -s -L https://nvidia.github.io/nvidia-docker/gpgkey | sudo apt-key add -
curl -s -L https://nvidia.github.io/nvidia-docker/$distribution/nvidia-docker.list | sudo tee /etc/apt/sources.list.d/nvidia-docker.list

sudo apt-get update && sudo apt-get install -y nvidia-container-toolkit
sudo systemctl restart docker
```

2. You need to load your s3 bucket

3. The running/working directory is assumed to be the root directory - production research

4. Run the relevant docker commands - see `serving/docs/tf_commands.md`

5. In your script, set the right port based on the docker command

6. Run the script

