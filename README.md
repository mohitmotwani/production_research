sudo docker run --rm -it -p 9090:9090 \
--name prom \
-v /home/mohitm/repos/production_research/serving/configs/input/prometheus.yml:/etc/prometheus/prometheus.yml \
-v /home/mohitm/repos/production_torch_serve/ssl/ca-cert.pem:/etc/prometheus/ca-cert.pem \
-v /home/mohitm/repos/production_torch_serve/ssl/client.cert.pem:/etc/prometheus/client.cert.pem \
-v /home/mohitm/repos/production_torch_serve/ssl/client.key.pem:/etc/prometheus/client.key.pem \
prom/prometheus